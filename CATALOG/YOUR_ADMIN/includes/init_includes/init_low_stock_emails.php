<?php
  if (!defined('IS_ADMIN_FLAG')) {
    die('Illegal Access');
  }   
 
if(nmx_check_field(TABLE_PRODUCTS,'products_low_stock')){
      

 $module_installer_directory =  DIR_FS_ADMIN.'includes/installers/low_stock_emails';
 $module_name = "Low Stock Emails"; 

 if(defined('LOW_STOCK_EMAILS_VERSION')) { $current_version =  LOW_STOCK_EMAILS_VERSION; } else { $current_version = "0.0.0"; }
  
 $installers = scandir($module_installer_directory, 1);
 
 $newest_version = $installers[0];
 $newest_version = substr($newest_version,0,-4);
 
 sort($installers);
 if(version_compare($newest_version, $current_version) > 0){
     foreach ($installers as $installer) {
         if(version_compare($newest_version, substr($installer,0,-4) ) >= 0 && version_compare($current_version, substr($installer,0,-4) ) < 0 ){
         include($module_installer_directory.'/'.$installer);
         $current_version = str_replace("_", ".", substr($installer,0,-4));
         $messageStack->add("Installed ".$module_name." V ".$current_version, 'success');
         }
     }     
 }
 
 // Update if requested
 if(defined('CONFIG_UPDATE_LOW_STOCK_LEVELS') && CONFIG_UPDATE_LOW_STOCK_LEVELS == 'true'){
     $changed_low_stock_levels = update_low_stock_emails_levels(CONFIG_LOW_STOCK_EMAILS_DAYS);
     $db->Execute("UPDATE ".TABLE_CONFIGURATION." SET configuration_value='false' WHERE configuration_key='CONFIG_UPDATE_LOW_STOCK_LEVELS'");
     if($changed_low_stock_levels != "false"){
        $messageStack->add("Stock Levels Updated", 'success');
        $messageStack->add('<a href="'.$changed_low_stock_levels.'">Click Here for updates of Low Stock Levels</a>', 'success');
     }
     else{
     $messageStack->add("No Stock Levels Changes", 'caution');    
     }
 }
}
else{
    $messageStack->add("Products Table is missing column 'products_low_stock' this is required for Low Stock Products Email to work", 'error');
}

 