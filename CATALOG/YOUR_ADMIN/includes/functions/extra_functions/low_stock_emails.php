<?php

function update_products_low_stock_level($product_id,$stock_level){
    global $db;
    $db->Execute("UPDATE ".TABLE_PRODUCTS." SET products_low_stock=".$stock_level." WHERE products_id=".$product_id);
}

function update_low_stock_emails_levels($days = '30'){
    global $db;
    $low_stock_updated = false;
    $file_name = "csv/low_products_stock_" . date("Y-m-d-H-i-s") . ".csv";
    $fp = fopen($file_name,'w');
    fputcsv($fp, array('Products Name','Products ID','New Low Stock Level'));
    fclose($fp);
    $products_list = $db->Execute("SELECT products_id,products_low_stock FROM ".TABLE_PRODUCTS." WHERE products_low_stock <> ".STOCK_REORDER_LEVEL);
    //Make sure everyhting has atleast minimums
    while(!$products_list->EOF)
    {
        if(STOCK_REORDER_LEVEL > $products_list->fields['products_low_stock']){
           $low_stock_updated = true;
                    $fp = fopen($file_name,'a');
                    fputcsv($fp, array(zen_get_products_name($products_list->fields['products_id']),$products_list->fields['products_id'],STOCK_REORDER_LEVEL));
                    fclose($fp);
                update_products_low_stock_level($products_list->fields['products_id'],STOCK_REORDER_LEVEL); 
        }
        $products_list->MoveNext();
    }
    $orders = $db->Execute("SELECT orders_id FROM ".TABLE_ORDERS." WHERE DATE(date_purchased) >= CURDATE() - INTERVAL ".$days." DAY");
    while(!$orders->EOF){
        $products_sold = $db->Execute("SELECT products_id, SUM(products_quantity) FROM ".TABLE_ORDERS_PRODUCTS." WHERE orders_id=".$orders->fields['orders_id']." GROUP BY products_id");
        // Cehck what was sold
        while(!$products_sold->EOF){
            $current_low_stock_level = npf_field_value($products_sold->fields['products_id'],'products_low_stock');
            if($products_sold->fields['SUM(products_quantity)'] > $current_low_stock_level){
                $low_stock_updated = true;
                    $fp = fopen($file_name,'a');
                    fputcsv($fp, array(zen_get_products_name($products_sold->fields['products_id']),$products_sold->fields['products_id'],$products_sold->fields['SUM(products_quantity)']));
                    fclose($fp);
                update_products_low_stock_level($products_sold->fields['products_id'],$products_sold->fields['SUM(products_quantity)']);
            }
            else if($current_low_stock_level > STOCK_REORDER_LEVEL){
                $low_stock_updated = true;
                    $fp = fopen($file_name,'a');
                    fputcsv($fp, array(zen_get_products_name($products_sold->fields['products_id']),$products_sold->fields['products_id'],STOCK_REORDER_LEVEL));
                    fclose($fp);
                update_products_low_stock_level($products_sold->fields['products_id'],STOCK_REORDER_LEVEL);
            
            }
            $products_sold->MoveNext();
        }
    $orders->MoveNext();
    }
    if($low_stock_updated == true){
       return $file_name;
    }
    else{
        return "false";    
    }
}

