<?php
$configuration = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION_GROUP . " WHERE configuration_group_title = 'Stock' ORDER BY configuration_group_id ASC;");
$configuration_group_id = $configuration->fields['configuration_group_id'];

$db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES
            ('Low Stock by Sales Version', 'LOW_STOCK_EMAILS_VERSION', '1.0.0', 'Version installed:', " . $configuration_group_id . ", 9991, NOW(), NOW(), NULL, NULL), 
            ('Low Stock Sales Look Back Days', 'CONFIG_LOW_STOCK_EMAILS_DAYS', '30', 'Number of Days to look back at the sales to calculate the low stock emails', " . $configuration_group_id . ", 9992, NOW(), NOW(), NULL, NULL),   
            ('Update Low Stock Levels for Products', 'CONFIG_UPDATE_LOW_STOCK_LEVELS', 'false', 'To update the Low Stock Levels for Products, set value below, select true on right, and Save Changes. Note that the value will change back to false each time.', " . $configuration_group_id . ", 9993, NOW(), NOW(), NULL, 'zen_cfg_select_option(array(\"true\", \"false\"),');");
